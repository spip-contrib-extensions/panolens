<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_panorama' => 'Ajouter ce panorama',

	// C
	'champ_titre_label' => 'Titre',
	'confirmer_supprimer_panorama' => 'Confirmez-vous la suppression de ce panorama ?',

	// I
	'icone_creer_panorama' => 'Créer un panorama',
	'icone_modifier_panorama' => 'Modifier ce panorama',
	'info_1_panorama' => 'Un panorama',
	'info_aucun_panorama' => 'Aucun panorama',
	'info_nb_panoramas' => '@nb@ panoramas',
	'info_panoramas_auteur' => 'Les panoramas de cet auteur',

	// R
	'retirer_lien_panorama' => 'Retirer ce panorama',
	'retirer_tous_liens_panoramas' => 'Retirer tous les panoramas',

	// S
	'supprimer_panorama' => 'Supprimer ce panorama',

	// T
	'texte_ajouter_panorama' => 'Ajouter un panorama',
	'texte_changer_statut_panorama' => 'Ce panorama est :',
	'texte_creer_associer_panorama' => 'Créer et associer un panorama',
	'texte_definir_comme_traduction_panorama' => 'Ce panorama est une traduction du panorama numéro :',
	'titre_langue_panorama' => 'Langue de ce panorama',
	'titre_logo_panorama' => 'Logo de ce panorama',
	'titre_objets_lies_panorama' => 'Liés à ce panorama',
	'titre_panorama' => 'Panorama',
	'titre_panoramas' => 'Panoramas',
	'titre_panoramas_rubrique' => 'Panoramas de la rubrique',
);
